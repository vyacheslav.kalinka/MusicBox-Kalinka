package ua.danit.MusicBox.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ua.danit.MusicBox.DAO.AlbumDao;
import ua.danit.MusicBox.entity.Album;


@RestController
public class AlbumsController {

  @Autowired
  private AlbumDao albumDao;


  @RequestMapping(path = "/albums", method = RequestMethod.PUT)
  public void addAlbum(@RequestBody Album album) {albumDao.save(album);}


  @RequestMapping(path = "/albums/{id}", method = RequestMethod.GET)
  public Album getById(@PathVariable int id) {return albumDao.findById(id).get();}


  @RequestMapping(path = "/albums", method = RequestMethod.GET)
  public Iterable<Album> getAll() {return albumDao.findAll();}

  @RequestMapping(path = "/albums/{idToRemove}", method = RequestMethod.DELETE)
  public void removeById(@PathVariable("idToRemove") int id) {albumDao.deleteById(id);}


  @RequestMapping(path = "/albums/{idToUpdate}", method = RequestMethod.POST)
  public void updateAlbum(@RequestBody Album album) {albumDao.save(album);}
}
