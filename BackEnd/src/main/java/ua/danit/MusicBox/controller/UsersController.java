package ua.danit.MusicBox.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ua.danit.MusicBox.DAO.UserDao;
import ua.danit.MusicBox.entity.User;

@RestController
public class UsersController {


    @Autowired
    private UserDao userDao;

    @RequestMapping(path = "/users", method = RequestMethod.GET)
    public Iterable<User> getAll() {return userDao.findAll();}

    @RequestMapping(path = "/users/{id}", method = RequestMethod.GET)
    public User getById(@PathVariable int id) {return userDao.findById(id).get();}

    @RequestMapping(path = "/users", method = RequestMethod.PUT)
    public void addUser(@RequestBody User user) {userDao.save(user);}

//    @RequestMapping(path = "/user/{idToRemove}", method = RequestMethod.DELETE)
//    public void removeById(@PathVariable("idToRemove") int id) {userDao.deleteById(id);}

//    @RequestMapping(path = "/user/{idToUpdate}", method = RequestMethod.POST)
//    public void updateUser(@RequestBody User user) {userDao.save(user);}

}
