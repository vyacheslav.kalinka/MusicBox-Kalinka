package ua.danit.MusicBox.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ua.danit.MusicBox.DAO.TrackDao;
import ua.danit.MusicBox.entity.Track;

@RestController
public class TracksController {

  @Autowired
  private TrackDao trackDao;

  @RequestMapping(path = "/tracks", method = RequestMethod.GET)
  public Iterable<Track> getAll() {return trackDao.findAll();}

  @RequestMapping(path = "/tracks/{id}", method = RequestMethod.GET)
  public Track getById(@PathVariable int id) {return trackDao.findById(id).get();}

  @RequestMapping(path = "/tracks/{idToRemove}", method = RequestMethod.DELETE)
  public void removeById(@PathVariable("idToRemove") int id) {trackDao.deleteById(id);}

  @RequestMapping(path = "/tracks", method = RequestMethod.PUT)
  public void addTrack(@RequestBody Track track) {trackDao.save(track);}

  @RequestMapping(path = "/tracks/{idToUpdate}", method = RequestMethod.POST)
  public void updateTrack(@RequestBody Track track) {trackDao.save(track);}
}
