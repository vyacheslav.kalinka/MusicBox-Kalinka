package ua.danit.MusicBox.DAO;

import org.springframework.data.repository.CrudRepository;
import ua.danit.MusicBox.entity.Album;

public interface AlbumDao extends CrudRepository <Album, Integer> {
  //void updateAlbum(int id);
}
