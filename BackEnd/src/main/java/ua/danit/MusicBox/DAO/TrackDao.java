package ua.danit.MusicBox.DAO;

import org.springframework.data.repository.CrudRepository;
import ua.danit.MusicBox.entity.Track;

public interface TrackDao extends CrudRepository<Track, Integer> {

  //void updateTrack(Track track);
}
