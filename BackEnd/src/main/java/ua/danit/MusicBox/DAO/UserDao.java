package ua.danit.MusicBox.DAO;

import org.springframework.data.repository.CrudRepository;
import ua.danit.MusicBox.entity.User;

public interface UserDao extends CrudRepository<User, Integer> {

}
