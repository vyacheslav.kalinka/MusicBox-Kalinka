package ua.danit.MusicBox.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "album")
public class Album {

  @Id
  @GeneratedValue(generator = "increment")
  @GenericGenerator(name = "increment", strategy = "increment")
  private int id;

  @Column(name = "name", length = 70, nullable = false)
  private String name;

  @Column(name = "cover", length = 200, nullable = false)
  private String cover;

  @OneToMany(mappedBy = "album")
  private List<Track> trackList;


  public Album() {}

  public Album(int id, String name, String cover) {
    this.id = id;
    this.name = name;
    this.cover = cover;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCover() {
    return cover;
  }

  public void setCover(String cover) {
    this.cover = cover;
  }

  @Override
  public String toString() {
    return "Album{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", cover='" + cover + '\'' +
        '}';
  }
}
