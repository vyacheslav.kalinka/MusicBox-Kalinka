package ua.danit.MusicBox.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "track")
public class Track {

  @Id
  @GeneratedValue(generator = "increment")
  @GenericGenerator(name = "increment", strategy = "increment")
  private int id;

  @Column(name = "name", length = 40, nullable = false)
  private String name;

  @Column(name = "mp3", length = 100, nullable = false)
  private String mp3;

  @ManyToOne
  @JoinColumn(name = "album_id", nullable = false)
  private Album album;

  public Track() {}

  public Track(int id, String name, String mp3) {

    this.id = id;
    this.name = name;
    this.mp3 = mp3;
  }


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getMp3() {
    return mp3;
  }

  public void setMp3(String mp3) {
    this.mp3 = mp3;
  }


}
