import React, { Component } from "react";

import {Switch, Route} from 'react-router-dom'
import Feed from './containers/AdminBox'
import Profile from './containers/Profile'
import Albums from './containers/Albums'

import './index.css'


class App extends Component {
    render(){

        // const linksArr = [
        // //     // {url: '/', text:'Main'},
        // //     // {url: '/profile', text:'profile'},
        // //     // {url: '/error', text:'Main'},
        // // ]
        return (<div>
            {/*<Navigation links={linksArr} mytitle='MaximkaCloud' />*/}
            <Switch>

                <Route exact path='/Admin' component={Feed}/>
                <Route path='/profile' component={Profile}/>
                <Route path='/api/albums' component={Albums}/>

                {/*<Route path='/main' component={Main}/>*/}

                {/*<Route path='/dog' component={Profile}/>*/}



            </Switch>

        </div>)
    }
}
export default App