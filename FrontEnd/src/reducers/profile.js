import {NEW_NAME} from "../actions/types";

const initialState = {}

function profileReducer (state = initialState, action){
    if (action.type === NEW_NAME) {
        return {...state, userName: action.payload}
    }

    return state
}

export default profileReducer