import {combineReducers} from 'redux'
import feed from './feed'
import profile from './profile'

const rootReducer = combineReducers({
    feed,
    profile
})

export default rootReducer