const initialState = []

function feedReducer (state = initialState, action){
    if(action.type === 'LOADED_FEED') {
        return action.payload;
    } else  if (action.type === 'NEW_FEED_TRACK'){
        return  [...state, action.payload]
    }
    return state
}

export default feedReducer