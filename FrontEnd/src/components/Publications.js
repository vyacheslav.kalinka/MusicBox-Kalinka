import React from 'react'
import PropTypes from 'prop-types'

class Publications extends React.Component {
    render() {
        return (
            <ul className='publications'>
                {this.props.imgs.map(id=><li key={id}><img src={`/posts/posts_00${id}.mp3`} alt={id + ' mp3'}/></li>)}
            </ul>
        )
    }
}

Publications.propTypes = {
    imgs: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired
}

export default Publications

//`/posts/posts_001${id}`
//'/posts/posts_001' + idz