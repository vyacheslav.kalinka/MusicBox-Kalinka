import React from 'react';
import {Switch, Route, Link} from 'react-router-dom'
import Publications from '../components/Publications'
import {connect} from "react-redux";
import {changeName} from "../actions/profileActions";

class Profile extends React.Component  {
    constructor () {
        super()

        this.state = {
            imgIds: []
        }
    }

    componentWillReceiveProps () {
        const url = this.props.location.pathname.indexOf('saved') > 0 ? '/mysaved.json': '/myposts.json'

        fetch(url)
            .then(res  => res.json())
            .then(data => {
                this.setState({imgIds: JSON.parse(data)})
            })
    }


    render() {
        const path = this.props.match.path

        return (
            <div>
                <h1>Profile</h1>
                <h2>Name: {this.props.userName}</h2>
                <span>Enter your name</span>
                <input onChange={(e) => this.props.changeName(e.target.value)}
                       type="text"/>
                <nav>
                    <Link to={path}>Liked tracks </Link>
                    <br/>
                    <Link to={`${path}/saved`}>saved</Link>
                </nav>
                <Switch>
                    <Route exact path={path} component={()=><Publications imgs={this.state.imgIds}/>} />
                    <Route exact path={path+'/saved'} component={()=><Publications imgs={this.state.imgIds}/>} />
                </Switch>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    userName: state.profile.userName
})

const mapDispatchToProps = dispatch => ({
    changeName: (newName) => dispatch(changeName(newName))
})

export default connect(mapStateToProps, mapDispatchToProps) (Profile)