import {LOADED_FEED, NEW_FEED_TRACK, ALBUMS_LOADED} from "./types";

export const loadData = () => ({type: LOADED_FEED, payload: [1,2,3,4]})
export const addTrack = () =>  ({type:NEW_FEED_TRACK, payload: 10})

export const loadAlbums = () => dispatch => {
    fetch('/api/albums')
        .then(res => res.json())
        .then(data => dispatch({type:ALBUMS_LOADED, payload: data}))
}
